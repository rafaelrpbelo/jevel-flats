import React, { Component } from 'react';
import Settings from '../settings';

const { site_name } = Settings;

class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      toggled: false
    };
  }

  toggleMenuClass() {
    return this.state.toggled ? 'is-active' : '';
  }

  toggleMenu() {
    console.log(this.state.toggled);
  }

  render() {
    return (
      <nav className="navbar has-shadow is-light" aria-label='main navigation'>
        <div className="navbar-brand">
          <a className="navbar-item" href="https://bulma.io">
            <h1 className="title">{ site_name }</h1>
          </a>

          <a
            onClick={() => this.setState({ toggled: !this.state.toggled })}
            role="button"
            className="navbar-burger"
            aria-label="menu"
            aria-expanded="false"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div className={`navbar-menu navbar-end ${this.toggleMenuClass()}`}>
          <a href="#rooms" className="navbar-item is-tab is-hidden-mobile">
            <span className="icon"><i className="fa fa-bed"></i></span>
            &nbsp;
            Quartos
          </a>

          <a href="#address" className="navbar-item is-tab is-hidden-mobile">
            <span className="icon"><i className="fa fa-map-marker"></i></span>
            &nbsp;
            Localização
          </a>

          <a href="#booking" className="navbar-item is-tab is-hidden-mobile">
            <span className="icon"><i className="fa fa-book"></i></span>
            &nbsp;
            Faça sua reserva
          </a>
        </div>
      </nav>
    );
  }
}

export default Navbar;
